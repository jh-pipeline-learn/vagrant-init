# -*- mode: ruby -*-
# vi: set ft=ruby :

script_ssh = <<-SCRIPT
    sed -i 's/ChallengeResponseAuthentication no/ChallengeResponseAuthentication yes/g' /etc/ssh/sshd_config
    service ssh restart
SCRIPT

cluster = {
    "pipe1-jenkins" => {
        :box => "debian/buster64",
        :ip => "192.168.5.2",
        :mem => 3072,
        :cpus => 2,
        :script => "install_p1jenkins.sh"
    },
    "pipe1-srv-dev" => {
        :box => "debian/buster64",
        :ip => "192.168.5.3",
        :mem => 512,
        :cpus => 1
    },
    "pipe1-srv-state" => {
        :box => "debian/buster64",
        :ip => "192.168.5.4",
        :mem => 512,
        :cpus => 1
    },
    "pipe1-srv-prod" => {
        :box => "debian/buster64",
        :ip => "192.168.5.5",
        :mem => 512,
        :cpus => 1
    },
    "pipe1-bdd-postgres" => {
        :box => "debian/buster64",
        :ip => "192.168.5.6",
        :mem => 512,
        :cpus => 1,
        :script => "install_srvpostgresql.sh"
    },
    "pipe1-registry" => {
        :box => "debian/buster64",
        :ip => "192.168.5.7",
        :mem => 512,
        :cpus => 1,
        :script => "install_registry.sh"
    },
    "pipe1-gitlab" => {
        :box => "debian/buster64",
        :ip => "192.168.5.8",
        :mem => 4096,
        :cpus => 2,
        :script => "install_gitlab.sh"
    }
}

Vagrant.configure("2") do |config|
    cluster.each do |hostname, info|
        config.vm.define hostname do |cfg|
            cfg.vm.box = info[:box]
            cfg.vm.network :private_network, ip: info[:ip]
            cfg.vm.provider :virtualbox do |vb|
                vb.name = hostname
                vb.memory = info[:mem]
                vb.cpus = info[:cpus]
                # vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
                # vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
            end
            cfg.vm.provision :shell, inline: script_ssh
            if info.has_key?(:script)
                cfg.vm.provision :shell, path: info[:script]
            end
        end
    end
end
